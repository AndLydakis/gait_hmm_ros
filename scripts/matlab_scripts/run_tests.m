test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.3, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.4, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.5, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.6, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.7, 10)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.3, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.4, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.5, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.6, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.7, 30)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.3, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.4, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.5, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.6, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.7, 100)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.3, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.4, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.5, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.6, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.7, 50)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.3, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.4, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.5, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.6, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_with_ir_prox_chk.mat', 0.7, 150)
clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.3, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.4, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.5, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.6, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.7, 10)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.3, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.4, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.5, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.6, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.7, 30)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.3, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.4, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.5, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.6, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.7, 100)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.3, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.4, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.5, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.6, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.7, 50)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.3, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.4, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.5, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.6, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_combined_without_ir_prox_chk.mat', 0.7, 150)
clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.3, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.4, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.5, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.6, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.7, 10)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.3, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.4, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.5, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.6, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.7, 30)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.3, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.4, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.5, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.6, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.7, 100)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.3, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.4, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.5, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.6, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.7, 50)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.3, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.4, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.5, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.6, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_ir_prox_chk.mat', 0.7, 150)
clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.3, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.4, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.5, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.6, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.7, 10)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.3, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.4, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.5, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.6, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.7, 30)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.3, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.4, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.5, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.6, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.7, 100)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.3, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.4, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.5, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.6, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.7, 50)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.3, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.4, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.5, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.6, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_mocap_combined_without_ir_prox_chk.mat', 0.7, 150)
clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.3, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.4, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.5, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.6, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.7, 10)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.3, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.4, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.5, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.6, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.7, 30)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.3, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.4, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.5, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.6, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.7, 100)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.3, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.4, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.5, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.6, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.7, 50)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.3, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.4, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.5, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.6, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_anfis_without_ir_prox_chk.mat', 0.7, 150)
clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.3, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.4, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.5, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.6, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.7, 10)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.3, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.4, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.5, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.6, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.7, 30)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.3, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.4, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.5, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.6, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.7, 100)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.3, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.4, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.5, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.6, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.7, 50)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.3, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.4, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.5, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.6, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_without_ir_prox_chk.mat', 0.7, 150)
clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.3, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.4, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.5, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.6, 10)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.7, 10)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.3, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.4, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.5, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.6, 30)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.7, 30)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.3, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.4, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.5, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.6, 100)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.7, 100)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.3, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.4, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.5, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.6, 50)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.7, 50)
clear all

test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.3, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.4, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.5, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.6, 150)
clear all
test_anfis('/home/lydakis-local/bu_mat/fsr_mocap_anfis_with_ir_prox_chk.mat', 0.7, 150)
clear all
